﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(kisisel_gelisim.Startup))]
namespace kisisel_gelisim
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
